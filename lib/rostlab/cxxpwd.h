/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_CXXPWD
#define ROSTLAB_CXXPWD 1

#include <pwd.h>
#include <string>
#include <sys/types.h>

#include <rostlab/rostlab_stdexcept.h>

namespace bo = boost;

namespace rostlab {

struct cxx_passwd
{
  std::string pw_name;       /* username */
  std::string pw_passwd;     /* user password */
  uid_t  pw_uid;        /* user ID */
  gid_t  pw_gid;        /* group ID */
  std::string pw_gecos;      /* real name */
  std::string pw_dir;        /* home directory */
  std::string pw_shell;      /* shell program */

  inline          cxx_passwd(){};
  inline          cxx_passwd( const std::string& __pw_name, const std::string& __pw_passwd, uid_t __pw_uid, gid_t __pw_gid, const std::string& __pw_gecos, const std::string& __pw_dir, const std::string& __pw_shell ) :
    pw_name(__pw_name), pw_passwd(__pw_passwd), pw_uid(__pw_uid), pw_gid(__pw_gid), pw_gecos(__pw_gecos), pw_dir(__pw_dir), pw_shell(__pw_shell) {};
};

class uid_not_found_error : public runtime_error { public: uid_not_found_error( const std::string& what ) : runtime_error(what) {} };
class uname_not_found_error : public runtime_error { public: uname_not_found_error( const std::string& what ) : runtime_error(what) {} };

// namespace functions
inline uid_t      getpwnam_r( const std::string& __uname );
inline uid_t      getpwnam_r( const std::string& __uname, cxx_passwd& __passwd );
inline std::string     getpwuid_r( uid_t __uid );
inline std::string     getpwuid_r( uid_t __uid, cxx_passwd& __passwd );



inline std::string     getpwuid_r( uid_t __uid )
{
  cxx_passwd      pwd;
  return getpwuid_r( __uid, pwd );
}


inline std::string     getpwuid_r( uid_t __uid, cxx_passwd& __passwd )
{
  long int buflen = sysconf( _SC_GETPW_R_SIZE_MAX );
  char buf[buflen];
  struct passwd pwbuf;
  struct passwd *pwbufp;

  int _errno = getpwuid_r( __uid, &pwbuf, buf, buflen, &pwbufp );

  if( _errno ) throw runtime_error( strerror( _errno ) );

  if( pwbufp == NULL ) throw uid_not_found_error( bo::str( bo::format("uid '%d' not found") % __uid ) );

  __passwd = cxx_passwd( pwbuf.pw_name, pwbuf.pw_passwd, pwbuf.pw_uid, pwbuf.pw_gid, pwbuf.pw_gecos, pwbuf.pw_dir, pwbuf.pw_shell );

  return __passwd.pw_name;
}


inline uid_t      getpwnam_r( const std::string& __uname )
{
  cxx_passwd      pwd;
  return getpwnam_r( __uname, pwd );
}


inline uid_t      getpwnam_r( const std::string& __uname, cxx_passwd& __passwd )
{
  long int buflen = sysconf( _SC_GETPW_R_SIZE_MAX );
  char buf[buflen];
  struct passwd pwbuf;
  struct passwd *pwbufp;

  int _errno = getpwnam_r( __uname.c_str(), &pwbuf, buf, buflen, &pwbufp );

  if( _errno ) throw runtime_error( strerror( _errno ) );

  if( pwbufp == NULL ) throw uname_not_found_error( bo::str( bo::format("uname '%s' not found") % __uname ) );

  __passwd = cxx_passwd( pwbuf.pw_name, pwbuf.pw_passwd, pwbuf.pw_uid, pwbuf.pw_gid, pwbuf.pw_gecos, pwbuf.pw_dir, pwbuf.pw_shell );

  return pwbuf.pw_uid;
}




}; // namespace rostlab

#endif // ROSTLAB_CXXPWD
// vim:et:ai:ts=2:
